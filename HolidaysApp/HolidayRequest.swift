//
//  HolidayRequest.swift
//  HolidaysApp
//
//  Created by Eldar Haseljic on 4/30/20.
//  Copyright © 2020 Eldar Haseljic. All rights reserved.
//

import Foundation
import  UIKit

enum Holiday_error:Error{
    case noDataAvailable
    case canNotProcessData
}

struct HolidayRequest{
    let resourseUrl:URL
    let API_KEY = "73d64d22dc6486eb771308b00c7df9d58785021a"
    
    init(countryCode:String) {
        let date = Date()
        let format = DateFormatter()
        format.dateFormat = "yyyy"
        
        let current_year = format.string(from: date)
        let resourse_string = "https://calendarific.com/api/v2/holidays?api_key=\(API_KEY)&country=\(countryCode)&year=\(current_year)"
        
        guard let resourseUrl = URL(string: resourse_string) else {fatalError()}
        
        self.resourseUrl = resourseUrl
    }
    
    func getHolidays(completion: @escaping(Result<[HolidayDetail], Holiday_error>) -> Void){
        let dataTask = URLSession.shared.dataTask(with: resourseUrl) {data, _ , _ in
            guard let jsonData = data else {
                completion(.failure(.noDataAvailable))
                return
            }
            
            do{
                let decoder = JSONDecoder()
                let holidaysResponse = try decoder.decode(HolidayResponse.self, from: jsonData)
                let holidayDetails = holidaysResponse.response.holidays
                completion(.success(holidayDetails))
            }catch{
                completion(.failure(.canNotProcessData))
            }
        }
        dataTask.resume()
    }
}
