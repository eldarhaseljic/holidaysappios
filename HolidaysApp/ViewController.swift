//
//  ViewController.swift
//  HolidaysApp
//
//  Created by Eldar Haseljic on 4/30/20.
//  Copyright © 2020 Eldar Haseljic. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var shearcBar: UISearchBar!
    @IBOutlet weak var Holiday_table: UITableView!
    
    var list = [HolidayDetail](){
        didSet{
            DispatchQueue.main.async {
                self.Holiday_table.reloadData()
                self.navigationItem.title = "\(self.list.count) Holidays Found"
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        shearcBar.delegate = self
    }

    
}



extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
      return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        let holiday = list[indexPath.row]
        
        cell.textLabel?.text = holiday.name! + "(\(holiday.date?.iso))"
        // cell.detailTextLabel?.text = holiday.date.iso
        return cell
    }
    
    
}

extension ViewController:UISearchBarDelegate{
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard  let searchBarText = searchBar.text else { return}
        let holidayRequest = HolidayRequest(countryCode: searchBarText)
        holidayRequest.getHolidays{
            [weak self] result in
            switch result {
            case .failure(let error):
                print(error)
            case .success(let holidays):
                self?.list = holidays
                //print(holidays)
            }
        }
        
    }
}

