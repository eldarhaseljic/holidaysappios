//
//  Holiday.swift
//  HolidaysApp
//
//  Created by Eldar Haseljic on 4/30/20.
//  Copyright © 2020 Eldar Haseljic. All rights reserved.
//

import Foundation
import UIKit

struct HolidayResponse:Decodable {
    var response:Holidays
}

struct Holidays:Decodable {
    var holidays:[HolidayDetail]
}

struct HolidayDetail:Decodable{
    var name:String?
    var date:DateInfo?
}

struct DateInfo:Decodable {
    var iso:String?
}
